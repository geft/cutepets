package com.lifeof843.cutepets.home

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import com.lifeof843.cutepets.R
import com.lifeof843.cutepets.constant.EnumConstant.GalleryType
import com.lifeof843.cutepets.constant.ExtraConstant
import com.lifeof843.cutepets.core.CoreActivity
import com.lifeof843.cutepets.photo.PhotoActivity
import com.lifeof843.cutepets.setting.SettingActivity
import com.lifeof843.cutepets.util.FaveLoader
import kotlinx.android.synthetic.main.activity_home.*
import org.codechimp.apprater.AppRater

class HomeActivity : CoreActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_home)
        initGridView()
        initBottomBar()
        initAppRater()
    }

    override fun onResume() {
        super.onResume()
        resetFaveGrid()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.home, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.menu_setting -> goToSetting()
        }
        return true
    }

    private fun resetFaveGrid() {
        if (bottom_bar.currentTabId == R.id.tab_favorites) {
            initFaveGridView()
        }
    }

    private fun goToSetting() {
        startActivity(Intent(this, SettingActivity::class.java))
    }

    private fun initBottomBar() {
        bottom_bar.setOnTabSelectListener {
            when (it) {
                R.id.tab_gallery -> initGridView()
                R.id.tab_favorites -> initFaveGridView()
            }
        }
    }

    private fun initGridView() {
        grid_view.adapter = GalleryAdapter(this, GalleryType.ALL)
        grid_view.setOnItemClickListener { adapterView, view, position, id ->
            openPhoto(position, GalleryType.ALL)
        }
    }

    private fun initFaveGridView() {
        val faveList = FaveLoader(this).getKeys()

        grid_view.adapter = GalleryAdapter(this, GalleryType.FAVE, faveList)
        grid_view.setOnItemClickListener { adapterView, view, position, id ->
            openPhoto(position, GalleryType.FAVE)
        }
    }

    private fun initAppRater() {
        AppRater.app_launched(this)
        AppRater.setCancelable(false)
        AppRater.setDontRemindButtonVisible(false)
        AppRater.setNumLaunchesForRemindLater(3)
    }

    private fun openPhoto(position: Int, type: GalleryType) {
        val intent = Intent(this, PhotoActivity::class.java)
        intent.putExtra(ExtraConstant().ITEM_POSITION, position)
        intent.putExtra(ExtraConstant().GALLERY_TYPE, type)
        startActivity(intent)
    }
}
