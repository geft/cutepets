package com.lifeof843.cutepets.home

import android.app.Activity
import android.content.Intent
import com.anjlab.android.iab.v3.BillingProcessor
import com.anjlab.android.iab.v3.TransactionDetails
import com.lifeof843.cutepets.R
import com.lifeof843.cutepets.util.Decryption
import com.lifeof843.cutepets.util.PrefPurchase
import com.lifeof843.cutepets.util.ResourceUtil

/**
 * Created by Gerry on 13/01/2017.
 */
class Billing(
        private val activity: Activity) : BillingProcessor.IBillingHandler {

    private var processor: BillingProcessor? = null

    override fun onBillingError(errorCode: Int, error: Throwable?) {}

    override fun onBillingInitialized() {}

    override fun onProductPurchased(productId: String?, details: TransactionDetails?) {
        if (productId == unlockId()) {
            PrefPurchase(activity).savePurchase(true)
        }
    }

    override fun onPurchaseHistoryRestored() {}

    init {
        processor = BillingProcessor(activity, Decryption().getLicenseKey(), this)
    }

    fun release() {
        processor?.release()
    }

    fun handleActivityResult(requestCode: Int, resultCode: Int, data: Intent): Boolean {
        return processor!!.handleActivityResult(requestCode, resultCode, data)
    }

    fun handleUnlock() {
        if (isPlayStoreSupported()) {
            processor?.purchase(activity, unlockId())
        } else {
            showUnlockFail()
        }
    }

    private fun isPlayStoreSupported() = BillingProcessor.isIabServiceAvailable(activity) && processor!!.isOneTimePurchaseSupported

    private fun unlockId() = ResourceUtil(activity).getString(R.string.purchase_id_unlock)

    private fun showUnlockFail() {
        ResourceUtil(activity).showSnackBar(R.string.dialog_unlock_fail_message)
    }
}