package com.lifeof843.cutepets.home

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.lifeof843.cutepets.R
import com.lifeof843.cutepets.constant.EnumConstant.GalleryType
import com.lifeof843.cutepets.util.FaveLoader
import com.lifeof843.cutepets.util.FileLoader
import com.lifeof843.cutepets.util.PrefPurchase
import com.lifeof843.cutepets.util.ResourceUtil
import java.util.*


/**
 * Created by Gerry on 18/12/2016.
 */
class GalleryAdapter(
        private val context: Context,
        private val galleryType: GalleryType,
        private val faveList: List<String> = ArrayList<String>()) : BaseAdapter() {

    private val inflater = LayoutInflater.from(context)
    private val fileLoader = FileLoader(context)
    private val faveLoader = FaveLoader(context)

    override fun getCount(): Int {
        when (galleryType) {
            GalleryType.ALL -> {
                val max = ResourceUtil(context).getInt(R.integer.max_count)
                val min = ResourceUtil(context).getInt(R.integer.min_count)

                return if (PrefPurchase(context).isPurchased()) max else min
            }
            GalleryType.FAVE -> return faveLoader.getCount()
        }
    }

    override fun getItem(position: Int): Any {
        return position
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val view: View
        val holder: ViewHolder

        if (convertView == null) {
            view = inflater.inflate(R.layout.gallery_thumb, parent, false)
            holder = ViewHolder()
            holder.image = view.findViewById(R.id.image) as ImageView
            holder.position = view.findViewById(R.id.position) as TextView
            view.tag = holder
        } else {
            view = convertView
            holder = convertView.tag as ViewHolder
        }

        when (galleryType) {
            GalleryType.ALL -> load(holder, position)
            GalleryType.FAVE -> {
                val favePos = if (faveList.isEmpty()) position
                else faveList[position].toInt()

                load(holder, favePos)
            }
        }

        return view
    }

    private fun load(holder: ViewHolder, position: Int) {
        Glide.with(context)
                .load(fileLoader.getDrawableThumb(position))
                .into(holder.image)

        holder.position?.text = (position + 1).toString()
    }

    private class ViewHolder {
        var image: ImageView? = null
        var position: TextView? = null
    }
}