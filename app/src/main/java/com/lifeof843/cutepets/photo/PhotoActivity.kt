package com.lifeof843.cutepets.photo

import android.os.Bundle
import android.support.v4.view.ViewPager
import com.lifeof843.cutepets.R
import com.lifeof843.cutepets.constant.EnumConstant.GalleryType
import com.lifeof843.cutepets.constant.ExtraConstant
import com.lifeof843.cutepets.core.CoreActivity
import com.lifeof843.cutepets.util.AdLoader
import com.lifeof843.cutepets.util.FaveLoader
import kotlinx.android.synthetic.main.activity_photo.*
import java.util.*

/**
 * Created by Gerry on 05/01/2017.
 */
class PhotoActivity : CoreActivity() {

    private var position: Int = 0
    private var galleryType: GalleryType = GalleryType.ALL
    private var photoToolbar: PhotoToolbar? = null
    private var faveList: List<String> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (savedInstanceState != null) {
            position = savedInstanceState.getInt(ExtraConstant().ITEM_POSITION)
            galleryType = savedInstanceState.getSerializable(ExtraConstant().GALLERY_TYPE) as GalleryType
        } else {
            position = intent.getIntExtra(ExtraConstant().ITEM_POSITION, 0)
            galleryType = intent.getSerializableExtra(ExtraConstant().GALLERY_TYPE) as GalleryType
        }

        setContentView(R.layout.activity_photo)
        initViews()
    }

    private fun initViews() {
        photoToolbar = PhotoToolbar(this, toolbar)
        faveList = FaveLoader(this).getKeys()
        initViewPager(photoToolbar!!)
        refreshToolbar(position)
        AdLoader(this, ad_banner).loadAd()
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)

        outState?.putInt(ExtraConstant().ITEM_POSITION, position)
        outState?.putSerializable(ExtraConstant().GALLERY_TYPE, galleryType)
    }

    private fun initViewPager(photoToolbar: PhotoToolbar) {
        view_pager.adapter = PhotoPagerAdapter(this, photoToolbar, galleryType, faveList)
        view_pager.offscreenPageLimit = 2
        view_pager.currentItem = position
        view_pager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {}
            override fun onPageScrollStateChanged(state: Int) {}
            override fun onPageSelected(position: Int) {
                refreshToolbar(position)
            }
        })
    }

    private fun refreshToolbar(position: Int) {
        val currPosition: Int

        when (galleryType) {
            GalleryType.ALL -> currPosition = position
            GalleryType.FAVE -> currPosition = faveList[position].toInt()
        }

        updatePosition(currPosition)
        photoToolbar?.initToolbar(currPosition)
    }

    private fun updatePosition(position: Int) {
        this.position = position
    }
}