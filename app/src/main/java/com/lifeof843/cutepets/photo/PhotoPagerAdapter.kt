package com.lifeof843.cutepets.photo

import android.content.Context
import android.support.v4.view.PagerAdapter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.lifeof843.cutepets.R
import com.lifeof843.cutepets.constant.EnumConstant.GalleryType
import com.lifeof843.cutepets.util.FileLoader
import com.lifeof843.cutepets.util.PrefPurchase
import com.lifeof843.cutepets.util.ResourceUtil
import java.util.*

/**
 * Created by Gerry on 05/01/2017.
 */
class PhotoPagerAdapter(
        private val context: Context,
        private val photoToolbar: PhotoToolbar?,
        private val galleryType: GalleryType,
        private var faveList: List<String> = ArrayList()) : PagerAdapter() {

    override fun getCount(): Int {
        when (galleryType) {
            GalleryType.ALL -> {
                val max = ResourceUtil(context).getInt(R.integer.max_count)
                val min = ResourceUtil(context).getInt(R.integer.min_count)

                return if (PrefPurchase(context).isPurchased()) max else min
            }
            GalleryType.FAVE -> return faveList.count()
        }
    }

    override fun isViewFromObject(view: View?, `object`: Any?): Boolean {
        return view == `object`
    }

    override fun destroyItem(container: ViewGroup?, position: Int, `object`: Any?) {
        container?.removeView(`object` as View)
    }

    override fun instantiateItem(container: ViewGroup?, position: Int): Any {
        val view = LayoutInflater.from(context).inflate(R.layout.activity_photo_item, container, false)
        val image = view.findViewById(R.id.image) as ImageView
        image.setOnClickListener { photoToolbar?.toggleToolbarVisibility() }

        when (galleryType) {
            GalleryType.ALL -> initPhoto(position, image)
            GalleryType.FAVE -> initPhoto(faveList[position].toInt(), image)
        }

        container?.addView(view)

        return view
    }

    private fun initPhoto(position: Int, image: ImageView) {
        Glide.with(context)
                .load(FileLoader(context).getDrawableFull(position))
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .crossFade()
                .into(image)
    }
}