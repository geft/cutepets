package com.lifeof843.cutepets.photo

import android.content.Context
import android.content.DialogInterface
import android.support.v7.widget.Toolbar
import android.view.View
import com.lifeof843.cutepets.R
import com.lifeof843.cutepets.util.DialogHelper
import com.lifeof843.cutepets.util.PhotoSharer
import com.lifeof843.cutepets.util.ResourceUtil
import com.lifeof843.cutepets.util.WallpaperLoader

/**
 * Created by Gerry on 06/01/2017.
 */
class PhotoToolbar(
        private val context: Context,
        private val toolbar: Toolbar) {

    private var menuInflated: Boolean = false
    private val faveToggle = PhotoFave(context, toolbar)

    fun initToolbar(position: Int) {
        initToolbarInflate()
        initToolbarTitle(position)
        initToolbarMenu(position)
        faveToggle.initToolbarIcon(position)
    }

    private fun initToolbarInflate() {
        if (!menuInflated) {
            toolbar.inflateMenu(R.menu.photo)
            menuInflated = true
        }
    }

    private fun initToolbarTitle(position: Int) {
        toolbar.title = (position + 1).toString()
    }

    private fun initToolbarMenu(position: Int) {
        toolbar.setOnMenuItemClickListener {
            when (it.itemId) {
                R.id.menu_fave -> faveToggle.toggleFave(position)
                R.id.menu_share -> PhotoSharer(context, position).sharePhoto()
                R.id.menu_wallpaper -> openWallpaperDialog(position)
            }
            true
        }
    }

    private fun openWallpaperDialog(position: Int) {
        val data = DialogHelper.DialogData()
        val resourceUtil = ResourceUtil(context)

        data.title = resourceUtil.getString(R.string.dialog_wallpaper_title)
        data.message = resourceUtil.getString(R.string.dialog_wallpaper_message)
        data.listener = DialogInterface.OnClickListener { dialogInterface, i ->
            WallpaperLoader(context, position).setWallpaper()
        }

        DialogHelper(context).getAlertDialog(data).show()
    }

    fun toggleToolbarVisibility() {
        if (toolbar.isShown) {
            toolbar.visibility = View.GONE
        } else {
            toolbar.visibility = View.VISIBLE
        }
    }
}