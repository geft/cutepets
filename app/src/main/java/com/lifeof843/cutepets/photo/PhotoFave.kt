package com.lifeof843.cutepets.photo

import android.content.Context
import android.support.v7.widget.Toolbar
import com.lifeof843.cutepets.R
import com.lifeof843.cutepets.util.FaveLoader
import com.lifeof843.cutepets.util.ResourceUtil

/**
 * Created by Gerry on 07/01/2017.
 */

class PhotoFave(val context: Context, val toolbar: Toolbar) {

    private val faveLoader = FaveLoader(context)

    fun initToolbarIcon(position: Int) {
        val isFave = isFave(position)

        if (isFave) setFaveTrue()
        else setFaveFalse()
    }

    fun isFave(position: Int) = faveLoader.isFave(position)

    fun setFaveTrue() {
        toolbar.menu.findItem(R.id.menu_fave).icon =
                ResourceUtil(context).getDrawable(R.drawable.ic_favorite_white)
    }

    fun setFaveFalse() {
        toolbar.menu.findItem(R.id.menu_fave).icon =
                ResourceUtil(context).getDrawable(R.drawable.ic_favorite_border_white)
    }

    fun toggleFave(position: Int) {
        if (isFave(position)) {
            faveLoader.remove(position)
            setFaveFalse()
        } else {
            faveLoader.add(position)
            setFaveTrue()
        }
    }
}
