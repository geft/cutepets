package com.lifeof843.cutepets.util

import android.content.Context
import android.content.DialogInterface
import android.support.v7.app.AlertDialog
import com.lifeof843.cutepets.R

/**
 * Created by Gerry on 08/01/2017.
 */
class DialogHelper(val context: Context) {

    fun getAlertDialog(dialogData: DialogData): AlertDialog {
        return AlertDialog.Builder(context)
                .setTitle(dialogData.title)
                .setMessage(dialogData.message)
                .setPositiveButton(R.string.dialog_ok, dialogData.listener)
                .setNeutralButton(R.string.dialog_cancel, { dialog, which -> dialog.dismiss() })
                .create()
    }

    fun getInfoDialog(dialogData: DialogData): AlertDialog {
        return AlertDialog.Builder(context)
                .setTitle(dialogData.title)
                .setMessage(dialogData.message)
                .setNeutralButton(R.string.dialog_dismiss, { dialog, which -> dialog.dismiss() })
                .create()
    }

    class DialogData {
        var title: String = ""
        var message: String = ""
        var listener: DialogInterface.OnClickListener? = null
    }
}