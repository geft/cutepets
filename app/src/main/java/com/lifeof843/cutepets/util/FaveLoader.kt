package com.lifeof843.cutepets.util

import android.content.Context

/**
 * Created by Gerry on 07/01/2017.
 */
class FaveLoader(context: Context) {

    private val favePrefs = "FAVE_PREFS"
    private val prefs = context.getSharedPreferences(favePrefs, Context.MODE_PRIVATE)

    fun add(position: Int) {
        prefs.edit()
                .putBoolean(padZero(position), true)
                .apply()
    }

    fun remove(position: Int) {
        prefs.edit()
                .remove(padZero(position))
                .apply()
    }

    fun isFave(position: Int): Boolean {
        return prefs.getBoolean(padZero(position), false)
    }

    fun getCount(): Int {
        return prefs.all.keys.size
    }

    fun getKeys(): List<String> {
        return prefs.all.keys
                .toList()
                .sorted()
                .map { removeZeroPad(it) }
    }

    fun clear() {
        prefs.edit().clear().apply()
    }

    private fun removeZeroPad(string: String): String {
        val trimmedString = string.trimStart('0')

        if (trimmedString.isEmpty()) return "0" else return string
    }

    private fun padZero(position: Int): String {
        return position.toString().padStart(3, '0')
    }
}