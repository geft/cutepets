package com.lifeof843.cutepets.util

/**
 * Created by Gerry on 12/01/2017.
 */
class Decryption {

    private val licenseKey = "ADIQIuS9lARHJDTgRGb1JawaBB7r5qtDR5FQM5QDWiJRdyYtx3DsVRcsNMSyN1GrcALJqfhef/t4lr7FGQ1tMLXbU0EQ94F0lao4Bf8Q/42Z9HBP1GZ63/tVdFdVTDIbNHnK4+KJ0ZcA/bKO3+D4yDxlRzg4T3tcjzCqmhdw/rFSblw6FYBGrYpvUQtC9+I/TH9KDhCsIV5ItzvJIeWTMofXdibpdhjlWNFIPRuMpqIWd4ZGQ/FCo82S4kQqWpeTDUOOPk+ZQ8JWRLem8KxwGODtTPp+jYVp6MntJ6tZhwzzia75WDbuvvRAzysvkzxp5PFGSx7a3ZSF/n0OLoJdOg2yiAEQACKgCBIIMA8QACOAAFEQAB0w9GikhqkgBNAjIBIIM"
    private val final3 = "QAB"

    fun getLicenseKey(): String {
        return StringBuilder(licenseKey).reverse().toString() + final3
    }
}