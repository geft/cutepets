package com.lifeof843.cutepets.util

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory

/**
 * Created by Gerry on 05/01/2017.
 */
class FileLoader(val context: Context) {

    private val typeThumb = "thumb"
    private val typeFull = "full"

    fun getDrawableThumb(position: Int): Int {
        return getDrawable(position, typeThumb)
    }

    fun getDrawableFull(position: Int): Int {
        return getDrawable(position, typeFull)
    }

    fun getBitmapFull(position: Int): Bitmap {
        return BitmapFactory.decodeResource(
                context.resources, getDrawableFull(position))
    }

    private fun getDrawable(position: Int, type: String): Int {
        return context.resources.getIdentifier(
                type + paddedPos(position + 1),
                "drawable",
                context.packageName)
    }

    private fun paddedPos(position: Int): String {
        val str = position.toString()
        return str.padStart(3, '0')
    }
}