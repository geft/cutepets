package com.lifeof843.cutepets.util

import android.app.Activity
import android.content.Context
import android.graphics.drawable.Drawable
import android.net.ConnectivityManager
import android.support.annotation.*
import android.support.design.widget.Snackbar
import android.support.v4.content.ContextCompat
import android.widget.Toast


/**
 * Created by Gerry on 18/12/2016.
 */
class ResourceUtil(private val context: Context) {

    fun getString(@StringRes resId: Int): String {
        return context.getString(resId)
    }

    fun getQuantityString(@PluralsRes resId: Int, quantity: Int): String {
        return context.resources.getQuantityString(resId, quantity)
    }

    fun getStringArray(@ArrayRes resId: Int): Array<String> {
        return context.resources.getStringArray(resId)
    }

    fun getDrawable(@DrawableRes resId: Int): Drawable {
        return ContextCompat.getDrawable(context, resId)
    }

    fun getInt(@IntegerRes resId: Int): Int {
        return context.resources.getInteger(resId)
    }

    fun showToast(text: String?) {
        if (text != null) {
            Toast.makeText(context, text, Toast.LENGTH_SHORT).show()
        }
    }

    fun showToast(stringId: Int) {
        Toast.makeText(context, getString(stringId), Toast.LENGTH_SHORT).show()
    }

    fun showSnackBar(stringId: Int) {
        val root = (context as Activity).window.decorView.rootView
        Snackbar.make(root, stringId, Snackbar.LENGTH_SHORT).show()
    }

    fun isNetworkConnected(): Boolean {
        val manager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        return manager.activeNetworkInfo != null
    }
}