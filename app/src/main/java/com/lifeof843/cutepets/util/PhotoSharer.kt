package com.lifeof843.cutepets.util

import android.content.Context
import android.content.Intent
import android.net.Uri
import com.lifeof843.cutepets.R
import java.io.File

/**
 * Created by Gerry on 05/01/2017.
 */
class PhotoSharer(
        private val context: Context,
        private val position: Int) {

    private val uriRoot = "android.resource://"
    private val intentType = "image/jpeg"

    fun sharePhoto() {
        val imageUri = Uri.parse(
                uriRoot + context.packageName + File.separator +
                        FileLoader(context).getDrawableFull(position)
        )
        val intent = shareIntent(imageUri)

        context.startActivity(Intent.createChooser(
                intent, ResourceUtil(context).getString(R.string.intent_share)))
    }

    private fun shareIntent(imageUri: Uri?): Intent {
        val intent = Intent(Intent.ACTION_SEND)
        intent.type = intentType
        intent.putExtra(Intent.EXTRA_STREAM, imageUri)
        return intent
    }
}