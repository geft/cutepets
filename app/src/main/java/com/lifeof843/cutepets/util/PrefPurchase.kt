package com.lifeof843.cutepets.util

import android.content.Context
import android.content.SharedPreferences

/**
 * Created by Gerry on 12/01/2017.
 */
class PrefPurchase(
        private val context: Context) {

    private val prefPurchase = "PREF_PURCHASE"
    private val purchaseKey = "PURCHASE_KEY"

    private fun getSharedPref(): SharedPreferences {
        return context.getSharedPreferences(prefPurchase, Context.MODE_PRIVATE)
    }

    fun savePurchase(isPurchased: Boolean) {
        getSharedPref().edit()
                .putBoolean(purchaseKey, isPurchased)
                .apply()
    }

    fun isPurchased(): Boolean {
        return getSharedPref().getBoolean(purchaseKey, false)
    }
}