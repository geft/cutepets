package com.lifeof843.cutepets.util

import android.app.WallpaperManager
import android.content.Context
import android.graphics.Bitmap
import com.lifeof843.cutepets.R
import rx.Observable
import rx.schedulers.Schedulers
import java.io.IOException


/**
 * Created by Gerry on 06/01/2017.
 */
class WallpaperLoader(val context: Context, val position: Int) {

    fun setWallpaper(): Boolean {
        val manager = WallpaperManager.getInstance(context)

        try {
            Observable.fromCallable { manager.setBitmap(getBitmap()) }
                    .subscribeOn(Schedulers.computation())
                    .subscribe()

            return true
        } catch (e: IOException) {
            e.printStackTrace()
            ResourceUtil(context).showToast(R.string.wallpaper_error)
        }

        return false
    }

    private fun getBitmap(): Bitmap {
        return FileLoader(context).getBitmapFull(position)
    }
}