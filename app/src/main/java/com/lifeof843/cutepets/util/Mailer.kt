package com.lifeof843.cutepets.util

import android.content.Context
import android.content.Intent
import android.net.Uri
import com.lifeof843.cutepets.R


/**
 * Created by Gerry on 20/01/2017.
 */
class Mailer(
        private val context: Context) {

    private val mailAddress = "843apps@gmail.com"
    private val mailSubject = "RE: " + context.packageName

    fun sendFeedback() {
        try {
            sendEmail()
        } catch (e: Exception) {
            showFailure()
        }
    }

    private fun sendEmail() {
        val sendIntent = Intent(Intent.ACTION_VIEW, Uri.parse("mailto:" + mailAddress))
        sendIntent.putExtra(Intent.EXTRA_SUBJECT, mailSubject)
        context.startActivity(sendIntent)
    }
    private fun showFailure() {
        ResourceUtil(context).showSnackBar(R.string.feedback_fail)
    }
}