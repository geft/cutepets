package com.lifeof843.cutepets.util

import android.content.Context
import android.view.View
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.AdView
import com.lifeof843.cutepets.R

/**
 * Created by Gerry on 16/01/2017.
 */
class AdLoader(
        private val context: Context,
        private val adView: AdView) {

    fun loadAd() {
        if (isUnlocked()) {
            adView.visibility = View.GONE
        } else {
            adView.visibility = View.VISIBLE
            adView.loadAd(adRequest())
        }
    }

    private fun isUnlocked() = PrefPurchase(context).isPurchased()

    private fun adRequest(): AdRequest {
        val requestBuilder = AdRequest.Builder()

        for (str in ResourceUtil(context).getStringArray(R.array.ad_keyword)) {
            requestBuilder.addKeyword(str)
        }

        return requestBuilder.build()
    }
}