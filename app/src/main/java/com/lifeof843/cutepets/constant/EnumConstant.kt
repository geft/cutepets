package com.lifeof843.cutepets.constant

/**
 * Created by Gerry on 08/01/2017.
 */
class EnumConstant {

    enum class GalleryType {
        ALL, FAVE
    }
}