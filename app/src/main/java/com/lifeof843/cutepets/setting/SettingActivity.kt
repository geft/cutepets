package com.lifeof843.cutepets.setting

import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.view.View.GONE
import android.view.View.VISIBLE
import com.lifeof843.cutepets.BuildConfig
import com.lifeof843.cutepets.R
import com.lifeof843.cutepets.core.CoreActivity
import com.lifeof843.cutepets.home.Billing
import com.lifeof843.cutepets.util.*
import kotlinx.android.synthetic.main.activity_setting.*

/**
 * Created by Gerry on 17/01/2017.
 */
class SettingActivity : CoreActivity() {

    var billing: Billing? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_setting)

        billing = Billing(this)
        initVersion()
        initListeners()
        initState()
        initUnlockDesc()
    }

    private fun initListeners() {
        unlock_app.setOnClickListener { performUnlock() }
        clear_fave.setOnClickListener { showClearFaveDialog() }
        send_feedback.setOnClickListener { sendFeedback() }
    }

    private fun sendFeedback() {
        Mailer(this).sendFeedback()
    }

    private fun performUnlock() {
        billing?.handleUnlock()
    }

    private fun initState() {
        if (PrefPurchase(this).isPurchased()) {
            layout_purchase.visibility = GONE
        } else {
            layout_purchase.visibility = VISIBLE
        }
    }

    private fun initUnlockDesc() {
        val resource = ResourceUtil(this)

        val max = resource.getInt(R.integer.max_count)
        val min = resource.getInt(R.integer.min_count)
        val diff = max - min

        unlock_desc.text = String.format(resource.getString(R.string.menu_unlock_desc), diff)
        unlock_desc.visibility = VISIBLE
    }

    override fun onDestroy() {
        billing?.release()

        super.onDestroy()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {
        if (!billing!!.handleActivityResult(requestCode, resultCode, data))
            super.onActivityResult(requestCode, resultCode, data)

        initState()
    }

    private fun showClearFaveDialog() {
        val data = DialogHelper.DialogData()
        val resourceUtil = ResourceUtil(this)

        data.title = resourceUtil.getString(R.string.dialog_fave_clear_title)
        data.message = resourceUtil.getString(R.string.dialog_fave_clear_message)
        data.listener = DialogInterface.OnClickListener { dialogInterface, i ->
            FaveLoader(this).clear()
        }

        DialogHelper(this).getAlertDialog(data).show()
    }

    private fun initVersion() {
        app_version.text = String.format(
                ResourceUtil(this).getString(R.string.menu_version_format), BuildConfig.VERSION_NAME
        )
    }
}